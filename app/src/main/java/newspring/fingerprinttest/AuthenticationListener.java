package newspring.fingerprinttest;
public interface AuthenticationListener {
    void succeeded();
    void failed();
}
