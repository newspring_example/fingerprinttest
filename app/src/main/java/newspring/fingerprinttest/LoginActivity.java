package newspring.fingerprinttest;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, AuthenticationListener{

    @BindView(R.id.etLoginID)
    EditText etLoginID;
    @BindView(R.id.etLoginPW)
    EditText etLoginPW;
    @BindView(R.id.btLogin)
    Button btLogin;
    @BindView(R.id.btBiometricLogin)
    Button btBiometricLogin;
    @BindView(R.id.tvDescription)
    TextView tvDescription;

    FingerprintManagerCallback fingerprintManagerCallback;
    BiometricPromptCallback biometricPromptCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if(Build.VERSION.SDK_INT  > Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.P){
            fingerprintManagerCallback = new FingerprintManagerCallback(this);
            fingerprintManagerCallback.setAuthenticationListener(this);
        }else if(Build.VERSION.SDK_INT == Build.VERSION_CODES.P){
            biometricPromptCallback = new BiometricPromptCallback(this);
            biometricPromptCallback.setAuthenticationListener(this);
        }

        btBiometricLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btBiometricLogin:
                if(Build.VERSION.SDK_INT  > Build.VERSION_CODES.M && Build.VERSION.SDK_INT < Build.VERSION_CODES.P){
                    fingerprintManagerCallback.startListening();
                }else if(Build.VERSION.SDK_INT == Build.VERSION_CODES.P){
                    biometricPromptCallback.startListening();
                }
                break;
        }
    }
    @Override
    public void succeeded() {
        tvDescription.setText("onAuthenticationSucceeded");
    }

    @Override
    public void failed() {
        tvDescription.setText("onAuthenticationFailed");
    }
}
