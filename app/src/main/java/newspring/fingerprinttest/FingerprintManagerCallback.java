package newspring.fingerprinttest;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

@RequiresApi(Build.VERSION_CODES.M)
public class FingerprintManagerCallback extends FingerprintManager.AuthenticationCallback
        implements CancellationSignal.OnCancelListener {
    public static final String DEFAULT_KEY_NAME = "LOGIN_KEY";

    private FingerprintManager fingerprintManager;
    private CancellationSignal cancellationSignal;
    private Cipher cipher;

    public FingerprintManagerCallback(Context context) {
        fingerprintManager = context.getSystemService(FingerprintManager.class);
    }

    public boolean isFingerprintAuthAvailable() {
        return fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints();
    }

    public boolean init() {
        try {
            KeyGenerator keyGenerator = createkey(DEFAULT_KEY_NAME, true);

            cipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);

            cipher.init(Cipher.ENCRYPT_MODE, keyGenerator.generateKey());

            return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void startListening() {
        if (!isFingerprintAuthAvailable()) {
            return;
        }
        if(init()){
            cancellationSignal = new CancellationSignal();
            cancellationSignal.setOnCancelListener(this);
            fingerprintManager.authenticate(new FingerprintManager.CryptoObject(cipher),
                    cancellationSignal, 0 , this, null);
        }else{
            authenticationListener.failed();
        }
    }

    public void stopListening() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    private KeyGenerator createkey(String keyName, boolean invalidatedByBiometricEnrollment) throws Exception {
        KeyGenerator mKeyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                KeyProperties.PURPOSE_ENCRYPT |
                        KeyProperties.PURPOSE_DECRYPT)
                .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                .setUserAuthenticationRequired(true)
                .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
        }
        mKeyGenerator.init(builder.build());
        mKeyGenerator.generateKey();
        return mKeyGenerator;
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);

    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);

        authenticationListener.succedded();

    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();

        authenticationListener.failed();
    }

    @Override
    public void onCancel() {

    }

    AuthenticationListener authenticationListener;

    public void setAuthenticationListener(AuthenticationListener authenticationListener) {
        this.authenticationListener = authenticationListener;
    }
}
