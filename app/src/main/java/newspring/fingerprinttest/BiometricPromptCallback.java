package newspring.fingerprinttest;

import android.content.Context;
import android.content.DialogInterface;
import android.hardware.biometrics.BiometricPrompt;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Trace;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.annotation.RequiresApi;

import java.security.KeyPairGenerator;
import java.security.Signature;
import java.security.spec.ECGenParameterSpec;

@RequiresApi(Build.VERSION_CODES.P)
public class BiometricPromptCallback extends BiometricPrompt.AuthenticationCallback
        implements CancellationSignal.OnCancelListener {
    public static final String DEFAULT_KEY_NAME = "LOGIN_KEY";

    private Context context;
    private BiometricPrompt mBiometricPrompt;
    private CancellationSignal cancellationSignal;
    private Signature signature = null;
    private AuthenticationListener authenticationListener;

    public BiometricPromptCallback(Context context) {
        this.context = context;
        mBiometricPrompt = new BiometricPrompt.Builder(context)
                .setDescription("Description")
                .setTitle("Title")
                .setSubtitle("Subtitle")
                .setNegativeButton("Cancel", context.getMainExecutor(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .build();
    }

    public boolean init() {
        try {
            KeyPairGenerator keyPairGenerator = createkey(DEFAULT_KEY_NAME, true);
            signature = Signature.getInstance("SHA256withECDSA");
            signature.initSign(keyPairGenerator.generateKeyPair().getPrivate());

            return true;
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    public void startListening() {
        if(init()){
            cancellationSignal = new CancellationSignal();
            cancellationSignal.setOnCancelListener(this);
            mBiometricPrompt.authenticate(new BiometricPrompt.CryptoObject(signature),cancellationSignal, context.getMainExecutor(),this);
        }else{
            authenticationListener.failed();
        }
    }

    public void stopListening() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal = null;
        }
    }

    private KeyPairGenerator createkey(String keyName, boolean invalidatedByBiometricEnrollment) throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");

        KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                KeyProperties.PURPOSE_SIGN)
                .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                .setDigests(KeyProperties.DIGEST_SHA256,
                        KeyProperties.DIGEST_SHA384,
                        KeyProperties.DIGEST_SHA512)
                .setUserAuthenticationRequired(true)
                .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);

        keyPairGenerator.initialize(builder.build());

        return keyPairGenerator;
    }

    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        super.onAuthenticationError(errorCode, errString);

    }

    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        super.onAuthenticationHelp(helpCode, helpString);

    }

    @Override
    public void onAuthenticationSucceeded(BiometricPrompt.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);

        authenticationListener.succedded();
    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();

        authenticationListener.failed();
    }

    @Override
    public void onCancel() {

    }

    public void setAuthenticationListener(AuthenticationListener authenticationListener) {
        this.authenticationListener = authenticationListener;
    }
}
